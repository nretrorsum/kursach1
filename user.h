#ifndef USER_H
#define USER_H
#include <iostream>
#include <fstream>
#include <vector>
#include <sstream>
using namespace std;
class Users
{
    private:
       string name,surname,info;
       int age;
    public:
       int rating;
       void replace_org_history(string number, vector<string> lines){
         ifstream infile("C:/Users/1/EVERYTHING/Projects Sofia/Cursova/Organizations.txt");
         stringstream buffer;
         buffer<<infile.rdbuf();
         string content = buffer.str();
         infile.close();
         size_t start = content.find(number);
         size_t end = content.find("!!!", start);
         if (end == string::npos) {
        cerr << "End delimiter not found in content\n";
        return;
    }
         content.erase(start, end - start);
         for(const auto& line: lines){
            content.insert(start, line + "\n");
            start +=line.size() + 1;
         }
         ofstream outfile("C:/Users/1/EVERYTHING/Projects Sofia/Cursova/Organizations.txt");
         outfile<<content;
         outfile.close();
       }
       string replace_rating(int number){
        string final_rating_to_replace(10, '~');
        for(int i=0;i<number;i++){
            final_rating_to_replace[i] = '*';
        }
        final_rating_to_replace  += "(" + to_string(number) + "/10)";
        return final_rating_to_replace;
       }
       void change_rating(string number){
        ifstream infile("C:/Users/1/EVERYTHING/Projects Sofia/Cursova/Ratings.txt");
        char which_line = number[0];
        int final_line = which_line - '0';
        final_line--;
        vector <string> lines;
        string line;
        while(getline(infile, line)){
            lines.push_back(line);
        }
        infile.close();
        string first_rating = lines[final_line];
        int first_rating2 = stoi(first_rating);
        int result = (first_rating2 + rating)/2;
        string final_rating = to_string(result);
        lines[final_line] = final_rating;
        ofstream outfile("C:/Users/1/EVERYTHING/Projects Sofia/Cursova/Ratings.txt");
          for (const auto& l : lines) {
              outfile << l << std::endl;
        }
        outfile.close();
        infile.open("C:/Users/1/EVERYTHING/Projects Sofia/Cursova/Organizations.txt");
        string line_2;
        bool if_found_org = false;
        lines.clear();
        while(getline(infile, line_2)){
            if(line_2.rfind(which_line, 0)==0){
                if_found_org=true;
            }
            if(if_found_org){
                if(line_2.rfind("*", 0)==0){
                    lines.push_back(replace_rating(result));
                    break;
                }else{
                    lines.push_back(line_2);
                }
            }
        }
        replace_org_history(number, lines);
       }
       void set_name(string &name){
        this->name=name;
       }
       void set_age(int &age){
        this->age=age;
       }
       void set_surname(string &surname){
        this->surname=surname;
       }
       void set_info(string &info){
        this->info=info;
       }
       void set_rating(int &rating){
        this->rating=rating;
       }
       void data_in_file(){
        string filename = "C:/Users/1/EVERYTHING/Projects Sofia/Cursova/User additional info/" + surname + ".txt";
        ofstream outfile(filename);
        if(!outfile.is_open()){
            cerr<<"Error"<<endl;
        }else{
            outfile<<name<<endl;
            outfile<<surname<<endl;
            outfile<<age<<endl;
            outfile<<info<<endl;
            outfile<<rating<<endl;
        }
        outfile.close();
       }
};
#endif // USER_H