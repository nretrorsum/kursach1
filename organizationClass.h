#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include "user.h"
class UkrOrg : public Users{
    private:
       std::string targetline="";
       std::string filename;  
       std::vector<std::string> extracted_lines;
       std::string org_number = "";
       bool if_found=false;
    public:
        UkrOrg(){}
        bool find_and_extract(std::string number){
         targetline=number;
           std::ifstream infile("Organizations.txt");
           if (!infile.is_open()) {
              std::cerr << "Unable to open file.\n";
              return false;
           }
           std::string line;
           if_found = false;
           extracted_lines.clear();
           while (getline(infile, line)) {
             if (!if_found && line.find(number) == 0) {
                 std::cout << line << std::endl; 
                 if_found = true;
                 continue;
             } else if (if_found) {
               if (line == "!!!") {
               break; 
               }
              extracted_lines.push_back(line);
               }
            }
           infile.close();
           return if_found;
        }
        void displayText() const {
         if (if_found && !extracted_lines.empty()) {
           for (const auto &extracted_line : extracted_lines) {
              std::cout << extracted_line << std::endl;
            }
        } else if (!if_found) {
            std::cout << "There are no such organizations.\n";
         } else {
             std::cout << "No text found after the number.\n";
         }
        }
};